Standard Webpack template for Babel

{

  "name": "standard-babel-template",
  "version": "1.0.0",
  "description": "test out features of Buble transpiler",
  "main": "index.js",
  "scripts": {

     "start": "webpack",

     "s": "webpack-dev-server --inline --hot",
     "val": "webpack-validator webpack.config.js",
     "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "",
  "license": "ISC",
  "devDependencies": {

     "babel-core": "^6.17.0",

     "babel-loader": "^6.2.5",

     "babel-preset-es2015": "^6.16.0",

     "css-loader": "^0.25.0",

     "extract-text-webpack-plugin": "^2.0.0-beta.4",

     "html-webpack-plugin": "^2.22.0",

     "style-loader": "^0.13.1",

     "webpack": "^2.1.0-beta.25",

     "webpack-dev-server": "^2.1.0-beta.9",

     "webpack-validator": "^2.2.9"
  
  },
  "dependencies": {}
}